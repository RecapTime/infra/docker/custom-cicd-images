# Custom CI/CD Images for Cirrus CI and GitLab CI

[![pipeline status](https://gitlab.com/MadeByThePinsHub/infra/docker/custom-cicd-images/badges/main/pipeline.svg)](https://gitlab.com/MadeByThePinsHub/infra/docker/custom-cicd-images/-/commits/main)

This repository is where our Docker images for GitLab CI/CD usage are being crafted, along with some other stuff our infra team is doing here.
Please, read the README of the respective images before you use them on your own repos.

## What's we're currently maintaining

* [`bulldozer-abuild`](./docker/bulldozer-abuild) - An Alpine-based Docker image for maintaining an Alpine Linux package repository and also handling contributions, in one image.
  * We'll try to build this image across CPU arches that Alpine Linux currently supported as much as possible.
* [`mkdocs-material`](./docker/mkdocs-material/README.md) - Our own version of `squidfunk/mkdocs-material`, using latest version fo Python as possible.
  * To run the liveserver locally, run this command: `docker run --rm -it -p 8000:8000 -v ${PWD}:/docs madebythepinshub/mkdocs-material:liveserver liveserver`
  * WARNING: The `liveserver` tag is being deprecated and may be remove permanently from Docker Hub.

## Used for CI tests only

These images are for use in CI tests in this repo, sometimes even they're not published on container registries we're using.

* [`glcicd-test-image`](./docker/gitlab-cicd-script-tests/README.md) - An placeholder Docker image for testing different GitLab CI scripts we're using, not usually published on container registries.

## Contributing

See the `CONTRIBUTTING.md` for details for contributing images for us or improving an existing one.

This repository adheres to [The Pins Team Community Code of Conduct][coc], based on [Contributor Convenant v2.0][contributor-convenant], and [the Developer's Certificate of Origin].

[coc]: https://github.com/MadeByThePinsHub/policies/blob/master/CODE_OF_CONDUCT.md
[contributor-convenant]: https://www.contributor-covenant.org/version/2/0/code_of_conduct/
