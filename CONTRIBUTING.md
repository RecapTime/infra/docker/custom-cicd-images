# Contributing Guidelines

## Code of Conduct

All public activity, including the issue tracker and review threads, must follow [our Community Code of Conduct](./CODE_OF_CONDUCT.md) in all times

If you have any questions, reach out to an Community Maintainer or an Recap Time Squad memeber for help.

## BuildKit Setup

We use BuildKit to build Docker images, so if you want to build them from source in the same CPU arch as your machine, set `DOCKER_BUILDKIT` shell variable to `1` before running the `docker build` command.

Otherwise, to generate an JSON-based image metadata and do an multi-arch build locally, then you need to setup the buildkitd builder and QEMU ahead of time before using `scripts/build.sh`.
An quick DuckDuckGo search of `setup multi-arch docker image builds` should help.

## Linting Your Dockerfiles and Scripts

We use [ShellCheck](https://www.shellcheck.net) for scripts, especially entrypoint scripts and [Hadolint](https://github.com/hadolint/hadolint) for Dockerfiles, so make sure they're installed on your system (they should be installed on Gitpod) for linter to work in VS Code.

## Commit Message Format

We use Conventional Changelog format, originally from AngularJS, as our commit message format. To commit, run `yarn wizard:commit` anywhere in the repo (or `yarn commit` within the repo root) and follow prompts.

VS Code users should have atleast `vivaxy.vscode-conventional-commits` extension installed, probably prompted through the Recommended Extensions Install Prompt notification.
